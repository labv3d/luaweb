
--
-- process le template, avec les data {k1=v1,k2=v2,...}
--
--function insert(template,data) end 
--insert(t1,{y=23}) 
--os.exit()
--

local etlua = require "etlua"

local parser = etlua.Parser()

--
-- cache[template]
--
local templateCache={}
--
-- prend un texte et compte les lignes jusqu'a une certaine ligne n,  en  accumulant les  ^--[[p]]
--
local function line2pos(str,n)
    print("lookin for  line "..tostring(n))
    local  i=1
    local pos=0
    for line in str:gmatch(".-\n") do
        -- track if  line  contains position info
        pos=line:match("^--%[%[(%d-)%]%]") or pos
        --print("got",tostring(pos))
        print(i,blue(line))
        if i==n then return pos end
        i=i+1
    end
    return pos
end

local pos2line = function(str, pos)
  local line = 1
  for _ in str:sub(1, pos):gmatch("\n") do
    line = line + 1
  end
  return line
end

--
-- execute un  template fn, retourne la fonction  qui va  faire la  job
--
local function protect(fn,template,templatename,code)
    local myfn = function(...)
        local _tostring, _escape, _b, _b_i = ...
        setfenv(fn,getfenv())
        status,err=pcall(fn,_tostring,_escape,_b,_b_i)
        if status then
            return err   -- success! err is the buffer
        else
            -- fail a l'execution
            local line, msg = err:match("%[.-%]:(%d+): (.*)$")
            local pos=line2pos(code,_p)
            local tline=pos2line(template,pos)
            return nil,red("[template exec error]["..tostring(templatename).."]:L"..tline)..":"..tostring(msg)
        end
        return buf,err
    end
    return myfn
end

--
--  compile un template
--  utilise une cache des fonctions compilees
--
local function prepare(template,templatename)
    local code,err=parser:compile_to_lua(template)
    -- fail a la compilation du template?
    if not code then return nil,"[template syntax error]["..tostring(templatename).."]:"..err end
    code="_p=1\n"..code:gsub("\n%-%-%[%[(%d+)%]%]","\n--[[%1]] _p=%1 ")
    --print(blue(code))
    local fn,err
    fn,err = parser:load(code)
    if not fn then
        -- fail a la compilation lua
        --local line, msg = err:match("%[.-%]:(%d+): (.*)$")
        --print(yellow(err))
        --local pos=line2pos(code,tonumber(line))
        --local tline=pos2line(template,pos)
        --return nil,red("[template lua syntax error]["..tostring(templatename)..":L"..tline).."]:"..tostring(msg)
        return nil,red("[template lua syntax error]["..tostring(templatename).."]:")..yellow(err)
    end
    return protect(fn,template,templatename,code)
end


--
-- Seule fonction pour traiter un template.
-- ( utilisee  par insert()  et par T() )
--
-- data est  optionel, mais defini  des variables enplus  de global (_G)
-- templatename  est  pour  les messages  d'erreur  [optionel]
--
function insertTemplate(filename,data)
    local pfn=templateCache[filename]
    local err
    if not pfn then
        local txt=readfile(filename)
        if txt=="" then return nil,"Unable to read template "..filename end

        pfn,err=prepare(txt,filename)
        if not pfn then
            -- fail a la compilation
            return nil,err
        end
        -- conserve pour la prochaine fois...
        templateCache[filename]=pfn
    end
    -- execute!
    buf,err=parser:run(pfn,data)
    if not buf then
        --print("EXEC",err)
        return nil,err
    end
    return table.concat(buf)
end

--t0="template0 $\\frac{1}{x^2}$ et $$\\sum_i^n i^2$$ ,  a is <%- a  %> b is <%- b %> y is <%-  y %> hop2."
--buf,err=insertTemplate(t0,{y=1234})
--if buf then print(">>>"..buf.."<<<") else print("err:",err) end

--t2="template2 a is <%- a  %> b is <%- b %> y is <%-  y[''] %> hop2."
--t1="template y is <%- y.x %> |<%- insertTemplate(t2,{a=123,b=456,y=y}) %>| fin"
--urls={x=1,y=2,z=3}
--urls[""]="allo"
--buf,err=insertTemplate(t1,{y=urls})
--if buf then print(">>>"..buf.."<<<") else print("yo",err) end




