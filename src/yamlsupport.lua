--
-- yaml support
--

--
-- input .md is expected to be ---, then yaml, then ---, then markdown content.
-- the result will be {params=(all yaml),html=(markdown content)}
-- if the file does not exist, {} is returned, not nil
--


-- lire un yaml avec --- pour separer le contenu markdown
-- L est optionel. 
function readyaml(filename)
  local f=io.open(filename,"r")
  if f==nil then return {} end
	line=f:read("*line")
	local front={}
	if line and string.sub(line,1,3)=="---" then
		local txt={}
		while true do
			local line=f:read("*line")
			if string.sub(line,1,3)=="---" then break end
			txt[#txt+1]=line.."\n"
		end
		front=yaml.load(table.concat(txt))
        if front==nil then  front={}  end
		txt=nil
	end
	local content=f:read("*all")
	return {params=front,html=markdown(content)}
end


