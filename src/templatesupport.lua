--
-- template support
--
-- fonctions pour assister les templates
--

local contentIDX={}

--
--  construit une liste  indexee plutot qu'une liste par cle
--
function  initialize()
    contentIDX={}
    for _,v in  pairs(content)  do  contentIDX[#contentIDX+1]=v end
    return setmetatable(contentIDX,{__index=table})
end


--
-- site support
-- (fonctions utiles dans les template)
--
--[[
function countKinds()
	local kinds={}
	for k,v in ipairs(content) do
		kinds[v.kind]=(kinds[v.kind] or 0)+1
	end
	kinds[""]=nil
	kinds["file"]=nil
	for k,v in pairs(kinds) do
		print("got ",v,"of kind",k)
	end
	return kinds
end
--]]

-- retourne true si on a un match...
-- image/* match image/png, image/jpg
-- image match seulement image
-- toto match une table si la table contient un match quelqeus part
function myMatch(target,actual)
    if actual==nil then return false end
    if type(actual)=="table" then
        -- match un tableau... 1er submatch est ok
        for k,e in pairs(actual) do
            if myMatch(target,e) then return true end
        end
        return false
    else
        -- match une valeur
        if type(target)=="string" and target:sub(-1)=="*" then
            -- match or longer
            return string.find(tostring(actual),target:sub(1,-2),1,true)==1
        end
        -- exact match
        return string.find(tostring(actual).."X",tostring(target).."X",1,true)==1
    end
end

--[[
function myMatch(target,actual)
    local res=myMatchX(target,actual)
    if type(actual)~="table" then print("@@ match "..tostring(target).." with "..tostring(actual).." : "..tostring(res)) end
    return res
end
]]


--
-- deepIndex(key,t) split key with '.' to access sub tables
-- will return t[key] if key does not contain '.'
-- will return t[a][b] if key is "a.b"
--
function deepIndex(t,key)
    for k in string.gmatch(key, "(%a+)%.?") do
        if type(t)~="table" then return nil end
        t=t[k]
    end
    return t
end

--t={aa={bb=12,cc=34},aaa=1222}
--e=deepIndex(t,"aa.cc")
--print("e=",tostring(e))
--print(myMatch(34,t))

--
--
-- system de matching super cool
-- all() -> retourne tous les fichiers
-- none() -> retourne aucun fichier
-- and(key,val) -> garde seulement ce qui match key-val
-- or(key,val) -> ajoute tout qui match key-val
-- without(key,val) -> remove any that match key-val
--
-- on commence toujours par all() ou none()
-- on enchaine and() or() without()
--
-- le match est un deepIndex sur key, suivi d'un deepMatch
--
function tout()
    return  contentIDX
end
function table.first(t)
	local res=setmetatable({},{__index=table})
    if #t>0  then  res[1]=t[1] end
    return res
end
function table.et(t,key,val)
    --if type(val)~="table" then print("@@ ET key="..key.." val="..val) end
	local res=setmetatable({},{__index=table})
	for k,v in ipairs(t) do
        if myMatch(val,deepIndex(v,key)) then res[#res+1]=v end
    end
    return res
end
function table.sans(t,key,val)
	local res=setmetatable({},{__index=table})
	for k,v in ipairs(t) do
        if not myMatch(val,deepIndex(v,key)) then res[#res+1]=v end
    end
    return res
end
function table.sortby(t,key,direction)
    local direction=direction or "up"
	local res=setmetatable({},{__index=table})
    -- copy table, add key sortval for sorting
    local warn=false
	for k,v in ipairs(t) do
        local sv=deepIndex(v,key) or ""
        if type(sv)=="table" then sv="" warn=true end
        res[k]=v
        res[k].sortval=sv -- will be used to sort, remove accents!
    end
    if warn then print("**** SortBy key "..key.." is comparing tables ****") end
    local comp
    if direction=="up" then
        comp= function (a,b) return a.sortval<b.sortval end
    else
        comp= function (a,b) return a.sortval>b.sortval end
    end
    table.sort(res,comp)
	for k,v in ipairs(res) do v.sortval=nil end -- cleanup keys
    return res
end


--content={{aa=11,bb=111},{aa=22,bb=222},{aa=33,bb=333}}
--t=all():et("aa",22)
--content={{aa=11,bb={z="1111191",zz=2}},{aa=111,bb=222},{aa=33,bb=333}}
--t=all():et("aa","1*"):sans("bb.z","1*")
--for k,v in ipairs(t) do print(k,v.aa,v.bb) end




--
-- tous les fichiers qui sont d'un certaine family/id/file
-- (devrait etre trie mais bon)
-- family = xxx
-- id = xxx
-- file = toto.jpg
-- mime = mmm
-- tous les criteres dans le add/sub sont "and"
-- add{a,b} : add (a dn b)  -> succes add
-- sub(a,b) : remove (a and b) -> if suc then suc=false
--
-- le match doit etre exact,
-- sauf si la section se termine par *
--
-- ce qu'on cherche doit etre une string
-- si la page contient une table plutot qu'une string,
--   on match si n'importe quel element est une string et match
--
--[[
function select(add,sub)
	local res={}
	for k,v in ipairs(content) do
    local succes=true
    -- add
    for key,val in pairs(add) do
      if not myMatch(val,v[key]) then succes=false break end
    end
    if sub and succes then
      local remove=true
      for key,val in pairs(sub) do
        if not myMatch(val,v[key]) then remove=false break end
      end
      if remove then succes=false end
    end
    if succes then res[#res+1]=v end
	end
	--print("--K-- add "..t2s(add).." sub "..t2s(sub))
	--for i,v in ipairs(res) do print(i,t2s(v)) end
	--print("-----")
	return res
end
]]




--
-- tous les fichiers attache a un certain id
--
--[[
function allFilesWithId(myId)
	local res={}
	for k,v in ipairs(content) do
		if v.kind=="file" and v.id==myId then res[#res+1]=v end
	end
	--print("--F with id "..myId.." --")
	--for k,v in ipairs(res) do print(myId," ",k,"is",v.url) end
	--print("-----")
	return res
end
]]


-- retourne un seul contenu file nil est index.md
function find(family,id,file)
  for i,v in ipairs(content) do
    if string.find(v.family,family,1,true)==1 
    and string.find(v.id,id,1,true)==1 
    and string.find(v.file,(file or "index.md"),1,true)==1
    then
      return v
    end
  end
end

--
-- copie d'une table ordinaire (pas de metatable)
-- one key is never copied: this
--
function copyDeep(obj)
    if type(obj) ~= 'table' then return obj end
    local res = {}
    for k, v in pairs(obj) do
        if k~="this" then res[k] = copyDeep(v) end
    end
    return res
end


--
-- merge  2 tables, en faisant une  copie, et en ajoutant  this->table
--
function mergeDeep(a,b)
    local t=copyDeep(a)
    if not b or type(b)~='table' then return t end
    for k,v  in pairs(b) do
        if k~="this" then t[k] = copyDeep(v) end
    end
    t.this=t -- for providing our data to a called template
    return t
end



--
-- faire un template et retourner le resultat
-- le filename doit etre complet
--
function insert(filename,data,extradata)
    --print(">>> process insert  template "..filename)
	--local txt=readfile(filename)
    --if txt=="" then os.exit() end
	--local template,why = etlua.compile(txt)
	local template,why = insertTemplate(filename,mergeDeep(data,extradata))
    if template==nil then
        print("***>>> error in template "..filename.." error ***\n    "..why)
        os.exit(0)
	    return ""
    end
    --print("<<< done insert template "..filename)
    return template
end

--
-- template generation
-- filename doit etre sans le repertoire de base
-- ** USE THIS INSIDE TEMPLATES **
--
function T(partialfilename,data,extradata)
    return insert(inputDir.."/"..partialfilename,data,extradata)
end
function noT(partialfilename,data,extradata)
    return ""
end

--
-- tranforme une table en string
--
function t2s(t)
    if type(t)~="table" then return tostring(t) end
    local s={}
    for k,v in pairs(t) do s[#s+1]=k.."="..t2s(v) end
    return "{"..table.concat(s,",").."}"
end


--
-- valide que les variables suivantes  sont definies
--
function  need(...)
end


