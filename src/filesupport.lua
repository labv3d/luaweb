--
-- read/write files
--

--mime=require "mimetypes"

function readfile(filename)
	local f=io.open(filename,"rb")
	if not f then
		print("*** file "..filename.." not found")
		return ""
	end
	local txt=f:read("*all")
	f:close()
	return txt
end

function writefile(filename,content)
	createDirectories(filename)
	local f=io.open(filename,"wb")
    if not f then
        print(red("**** writefile unable >"..tostring(filename).."<"))
    else
        f:write(content)
        f:close()
        os.execute("chmod ugo+r "..filename)
    end
end


-- path doit  etre une  string
function scandir(basepath,fullpath,addfile)
  --print("scan ("..basepath.."/) "..fullpath)
  for file in lfs.dir(basepath.."/"..fullpath) do
    if file ~= "." and file ~= ".." then
      if fullpath=="" then f=file else  f=fullpath.."/"..file end
      if lfs.attributes(basepath.."/"..f,"mode") == "directory" then
        scandir(basepath,f,addfile)
      else
        addfile(basepath,f)
      end
    end
  end
end




-- return true if outname is the same as inname
-- based on size and modification
function isSameFile(inname,outname)
    local ina=lfs.attributes(inname)
    local outa=lfs.attributes(outname)
    if ina and outa and ina.size==outa.size and ina.modification < outa.modification then return true end
    return false
end

-- create all directories as needed by the path, (last is file)
function createDirectories(path)
    --print("CREATEDIR "..tostring(path))
    local dir
    local start,rest=path:match("^(%.?/?)(.*)")
    for s in string.gmatch(rest,"([^/]-)/") do
        if not dir then dir=start..s else dir=dir.."/"..s end
        if not lfs.attributes(dir,"mode") then lfs.mkdir(dir) end
    end
end

--createDirectories("./toto/aaa/bb/blub")
--createDirectories("/home/roys/toto/aa/bb/blub")
--createDirectories("toto/aa/bbblub")
--os.exit(0)

-- copy from in to out, create directory if needed
--
function doCopyFile(inpath,outpath)
	createDirectories(outpath)
    if isSameFile(inpath,outpath) then return end
    -- actual copy
	local fin=io.open(inpath,"r")
	local data=fin:read("*all")
	fin:close()
	local fout=io.open(outpath,"w+")
	if fout then
		fout:write(data)
		fout:close()
    else
        print(red("*** COPY unable to write to "..outpath))
        os.exit(0)
	end
end



