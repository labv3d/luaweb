
socket = require("socket")
--httpcode = require("httpcode")

mimetypes=require "mimetypes"



function main( p )
	local port
	local server

	if p ~= nil then 
		port = p 
	else  
		port = 80
	end

	server = assert(socket.bind("*", port))

	server_poll(server)
end

function server_poll( server )
	while 1 do
		client = server:accept()
		client:settimeout(60)
		local request, err = client:receive()

		if not err then
            -- if request is kill (via telnet), stop the server
            if request == "kill" then
                client:send("Ladle has stopped\n")
                print("Stopped")
                break
            else
                -- begin serving content
                serve(request)
            end
        end
	end
end

-- serve requested content
function serve(request)
    print("REQUEST",request)
    -- resolve requested file from client request
    local file = string.match(request, "%a- ([^?#]*)[?#]?.* HTTP.*$")
    -- if no file mentioned in request, assume root file is index.html.
    if file == '/' then
        file = "/index.html"
    end

    local fullname="/home/roys/v3d-local"..file
    print("reading  ",fullname)
        
    local mime = mimetypes.guess(fullname)
    print("FILE",fullname,mime)

    -- reply with a response, which includes relevant mime type
    if mime then
        print("OK  mime is ",mime)
        client:send("HTTP/1.1 200 OK\r\nServer: luaweb webserver\r\n")
        client:send("Content-Type: "..mime.."; charset=utf-8\r\n")
        client:send("Connection: close\r\n")
        client:send("\r\n")

        local f = io.open(fullname, "rb")
        if f then
            local content = f:read("*all")
            f:close()
            --print("sending  >>>"..content.."<<<")
            client:send(content)
        else
            err("file not  found : "..fullname)
        end
    else
        err("file has no mime : "..fullname)
    end

    -- done with client, close request
    client:close()
end


-- display error message and server information
function err(message)
    print("ERR",message)
    client:send(message)
end

main(8080)

