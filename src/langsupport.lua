
--
-- support  pour la langue
--
-- utilise:
-- config.langDefaut   "en"   (les fichier sans langue sont cette langue)
-- config.langPath     [en]= [fr]=  (seulement  des vraies langues)
--

--
-- split un nom de fichier en parties...
-- retourne directories , basefile (sans lang) , lang
-- SUPPOSE pas de  directory...
-- la langue L doit exister dans langpath[L]
-- default = true si il n'y a  pas  de lang specifique (aa.bb plutot que aa.fr.bb)
--
function splitPath(filepath,langpath,langdefaut)
  local t={}
  for s in string.gmatch(filepath,"([^/]+)") do t[#t+1]=s end
  f=t[#t]
  t[#t]=nil
  local D,L,S=string.match(f,"^(.*)%.([^%.][^%.]-)%.([^%.]-)$")
  local def=false
  if not L or L=="" then  --  no lang  specified
    L=langdefaut
    def=true
  else
    if langpath[L] then  --  lang  is explicit, remove from filename
      f=D.."."..S
    else
      -- on  suppose un aa.bb.cc qui doit etre preserve.
      --print("*** language >"..L.."< not  supported for file "..filepath.." (skip)")
      print("WARN *** file >"..filepath.."< has no valid langage. Using as is.")
      return table.concat(t,"/"),f,langdefaut,true
    end
  end
  return table.concat(t,"/"),f,L,def
end

--[[
function check(p)
    lang={fr="fr",en="en"}
    def="de"
    local  s="  :  "
    for i,v  in  ipairs({splitPath(p,lang,def)}) do s=s..tostring(v).."  |  " end
    print(p,s,#{splitPath(p,lang)});
end
check("v3d/content/a/b/x..md")
check("v3d/content/a/b/x.fr.md")
check("v3d/content/a/b/x.md")
check("v3d/content/a/b/style.min.css")
check("v3d/content/a/b/x")
check("x.fr.md")
check("x.md")
check("x")
os.exit(0)
]]

--
-- retourne (nom sans L),L pour un fichier a.L.b
-- allo/blub.fr.png   ->  allo/blub.png , fr
-- allo/blub.png      ->  allo/blub.png , defaut
--
function decoupe(path,langpath,defaut)
    -- D.L.S , ou tous nil  si pas de L
    -- L doit avoir au moins 1 char, pas  de max, normalement  2 char
    local D,L,S=string.match(path,"^(.*)%.([^%.][^%.]-)%.([^%.]-)$")
    if not L or L=="" or not langpath[L] then return path,defaut end
    return D.."."..S,L
end

-- Ajuste un path selon la langue
-- langpath['fr'] contient url prefix and path in URL
-- a b c d.suffix  -> a langpath[langDefaut] b c d.suffix ->  si langpath[""] ~= ""
-- a b c d.fr.suffix  ->   a langpath[fr] b c d.suffix  si langpath[fr] existe
--
function ajusteOutputDirLang(path,langpath,lang)
    print("AJUSTE "..table.concat(path,"/").." with lang="..tostring(lang))
    local P,L=decoupe(path[#path],langpath,lang)
    print("AJUSTE got P="..P.." L="..tostring(L).." lp="..tostring(langpath[L]))
    if not langpath[L] then
        -- unsupported language...  let it pass as is...
        return
    end
    if langpath[lang]~="" then table.insert(path,2,langpath[lang]) end
    path[#path]=P
    print("AJUSTE FIN "..table.concat(path,"/"))
end

--
-- tous  les URL pour  tous les languages
-- ressemble a ajusteLang... (qui defini le  fichier  output  pour  une langue)
-- ici on defini l' URL pour chaque  langue
--
-- file ,  dir , dir ,  dir  , file.fr.html
-- -> baseURL / fr / dir /  dir / dir /  fire.html
--
function makeURLs(path,baseURL,langpath,defaut)
    --print("MAKEURL "..baseURL)
    --for k,v in ipairs(path)  do  print("MAKEURL P",k,v)  end
    --for k,v in pairs(langpath) do print("MAKEURL L",k,v)  end
    local P,L=decoupe(path[#path],langpath,defaut)
    local urls={}
    for L,U in pairs(langpath)  do
        local out={}
        for i,v in ipairs(path) do out[i]=v end
        out[1]=baseURL
        out[#out]=P --  nom de fichier sans le .html ou .fr.html
        if U~="" then table.insert(out,2,U) end
        urls[L]=table.concat(out,"/")
        print("MAKEURL",L,urls[L])
    end
    return urls
end


--
-- traduction  simple
--
function P(t,lang)  return t[lang  or L] end


