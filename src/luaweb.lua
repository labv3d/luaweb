#!/usr/bin/lua5.1

function red(s) return string.char(27)..'[0;91m'..s..string.char(27)..'[0m' end
function green(s) return string.char(27)..'[0;92m'..s..string.char(27)..'[0m' end
function yellow(s) return string.char(27)..'[0;93m'..s..string.char(27)..'[0m' end
function blue(s) return string.char(27)..'[0;94m'..s..string.char(27)..'[0m' end

--print("The script was executed "..red("successfully")..", also in "..green('green').." and "..yellow('yellow').."!!!")



--
-- luaweb
--
-- v3d/config.md
--    langDefaut : lang pour une page .md sans indicateur  de langue (laisser vide si pas de langue  defaut)
--    langPath:  liste des langues a gerer  et ou sont les URL
--
-- Multilangue:
--   tous les fichiers sont associes a une langue.
--   un fichier .fr.md ou .en.md -> contenu de cette la langue  /fr/.../index.md
--
-- Regle de construction:
-- le  contenu est indexable par index
-- content est un element du site final, qui contient toutes les langues  [L].
--   element: id unique du contenu, sans egard a la langue  ... element:  family,id,file (on cherche la dessus dans  les templates)
--     family : premier repertoire seulement, donc content/pub/a/b/x ->  pub, content/x  -> "", meme chose pour layout/...
--     id : tous  les repertoires intermediaires ou "", donc content/pub/a/b/x ->  "a/b"
--     file: fichier dans aucune trace de lange. donc  /toto.fr.md -> toto.md ,  /toto.png ->  toto.png, /toto.html ->toto.html
--     -> on peut retrouver, en theorie, le fichier avec family/id/file, mais pas vraiment puisque ca  depend  de la langue
--     ->  si on a  /x.fr.md  et /x.en.md, on va  avoir un  seul  element  pour  les deux.
--     -> si  on  a /x.fr.md seulement, on va  avoir anglais base  sur le .fr.
--     mime: mime-type
--   inpath[L] : fichier source complet
--   template  : layout fichier template a appliquer sur le inpath pour le 'generer'  ou nil si pas besoin
--   outpath[L]  : emplacement du fichier output  genene ou copie
--   url[L]   : url final
-- 
--
-- Exemples: content/x.png 
--  family="",id="",file="x.png",mime="image",
--  inpath={fr=content/x.png,en=content/x.png}  template=nil
--  outpath={fr=outdir/fr/x.png,en=outdir/fr/x.png}
--  url={fr=outurl/fr/x.png,en=outurl/fr/x.png}
--    -> pour ne pas recopier, ca  pourrait aussi etre
--  outpath={fr=outdir/x.png,en=outdir/x.png}
--  url={fr=outurl/x.png,en=outurl/x.png}
--
-- Exemples: content/a/b/x.md  avec layout/a/b/x.any ou layout/x-.any ou  layout/a/x-.any (sans layout, le md n'est pas  recopie)
--  family="a",id="b",file="x.md",mime="markdown",
--  inpath={fr=content/a/b/x.md,en=content/a/b/x.md}  template=[a:b:x.any]
--  outpath={fr=outdir/fr/a/b/x.any,en=outdir/fr/a/b/x.any} si .md, alors  output path est  celui  du template.
--  outurl={fr=outurl/fr/a/b/x.any,en=outurl/fr/a/b/x.any}
--
-- scan le contenu de layout et build la table layout
--    -> layout va contenir seulement family,id,file,mime, inpath{...} outpath={} outurl={}
--    -> layout est indexable par id... family/id/file ... layout/a/x.fr.html  -> ""/a/"x.html" ,  x.en.html  ->  same  as x.fr.html
-- scan le contenu de content  et build la table content.
--
--
-- Structure d'un site web
-- v3d/content/
-- v3d/layout/
-- v3d/xyz/ -> ce qu'on veut, utilisable  par les template
--

-- copie d'une table ordinaire (pas de metatable)
function clone(t)
    if type(t) ~= 'table' then return t end
    local res = {}
    for k,v in pairs(t) do res[clone(k)] = clone(v) end
    return res
end



function splitFilename(strFilename)
  -- Returns the Path, Filename, and Extension as 3 values
  strFilename = strFilename.."."
  return strFilename:match("^(.-)([^\\/]-%.([^\\/%.]-))%.?$")
end

function chop(s,nb)
    if #s<=nb then return s end
    return s:sub(1,nb).."..."
end

function makeRef(family,id,file) return family..":"..id..":"..file end

function  dumpTable(t)
    for k,v in pairs(t) do
        local s=""
        if type(v)~="table" then
            s=tostring(v)
        elseif k=="template" then
            s=makeRef(v.family,v.id,v.file)
        else
            for j,x in pairs(v) do
                if k=="html" then  
                    s=s.."["..j.."=...]"
                else
                    s=s.."["..j.."="..tostring(x).."]"
                end
            end
        end
        print("  "..k.." = "..s)
    end
end

-- local path to find other files
local base=splitFilename(arg[0])
--print("luaweb is at ",base)

package.path=package.path..";"..base.."?.lua"
--print(package.path)

--i18n=require "i18n"
--etlua = require "etlua"
yaml = require "yaml"
markdown = require "markdown"
lfs=require "lfs"
mimetypes=require "mimetypes"


-- etlua : https://github.com/leafo/etlua

--
--  local  packages
--
require "yamlsupport"
require "filesupport"
require "templating-etlua"
require "templatesupport"
require "langsupport"

--
-- luaweb.lua 
--
i=1
while i<=#arg do
    print("checking  arg "..arg[i])
    if arg[i]=="-h" then
        print(arg[0]," [-h] [-flush] -i source [-o public -url baseURL] [-profil local|online|roys|...]")
        print("   will read web site in source directory (source/content, source/layout,...)")
        print("   and put it in public directory.")
        print("   A file source/content/aa/bb/cc will be copied to public/aa/bb/cc")
        print("   A file source/layout/aa/bb/cc.x will be copied to public/aa/bb/cc.x,  or processed as template  if public/aa/bb/cc.md exist")
        print("   A file source/anything-else... will NOT be copied.")
        print("   -profil x: select which parameter from config.md are used for outputDir and baseURL and others....")
        print("   Templating:")
        print("    - a file source/content/xyz.md will not be copied. It will  only provide data to a layout template if required.")
        print("    - a file source/layout/xyz.html will go through a template compiler with data provided by file source/content/xyz.md, otherwise  if will be copied.")
        print("    - a file source/layout/xyz-.html will used to template any content  file  below that has  no  matching template, like  source/content/aa/bb/xyz.md")
        print("    - a template in layout/ will only be executed if there is a matching file in content (or below if its name *-.html )")
        print("   MultiLingual:")
        print("    - a template")
        print("   Content structure:")
        print("    source/content/publications/unstructured/index.md -> family is publications, id is unstructured, file is index.md")
        print("    source/content/publications/aa/bb/unstructured/index.md -> family is publications/aa/bb")
        print("   Layout variables/parameters:")
        print("      config : yaml from source/config,md")
        print("      params : data from current content")
        print("      content : markdown from current content")
        print("      + all other global variables (please do not use)")
        print("   Layout selection functions from content:")
        print("      - tout()   : all content")
        print("      - et(key,val) : keep  only if [key]== val")
        print("      - sans(key,val) : keep  only if [key] ~= val")
        print("      - sortby(key,up|down) : sort all  result  by key")
        print("      (key can be a  to get [a], or a.b  to get [a][b]")
        print("   ")
        os.exit(0)
    elseif arg[i]=="-profil" and i+1<=#arg then
        profilParam=arg[i+1]
        i=i+1
    elseif arg[i]=="-i" and  i+1<=#arg then
        inputDir=arg[i+1]
        i=i+1
    elseif arg[i]=="-o" and i+1<=#arg then
        outputDir=arg[i+1]
        i=i+1
    elseif arg[i]=="-url" and i+1<=#arg then
        baseURL=arg[i+1]
        i=i+1
    elseif arg[i]=="-flush" then
        flush=true
    else
        print("unknown arg "..arg[i])
        os.exit(0)
    end
    i=i+1
end


--inputDir=inputDir or "source"
--outputDir=outputDir or "public"

if not inputDir then
    print("you must  specific input directory  (-i)")
    os.exit(0)
end


print("=== input directory : ",inputDir)


-- return family, id, file, lang for this path
-- path is base / content|layout / family / id / file
-- base/content/a/b/c/file -> family=a id=b/c
-- xxxxx.fr.md ->  lang=fr
-- xxxxx.md -> lang=""
function XXXanalyzePath(path)
    local P,L=decoupe(path[#path],config.langPath,config.langDefaut)
	if #path>=5 then
        -- base/content/authors/sebastien/.../index.md
		return path[3],table.concat(path,"/",4,#path-1),P,L
	elseif #path==4 then
		-- base/content/authors/index.md
		return path[3],"",P,L
	else
		-- base/content/index.md
		return "","",P,L
	end
end


--
--remplace file.any -> file.ext
--retourne le nouveau nom, et  1 ou  0  si ok/fail
-- ext doit contenir le .
--
function replaceExtension(file,ext)
    return file:gsub("(.*)%.[^%.]-$","%1"..ext)
end
function getExtension(file)
    return file:match(".*%.([^%.]-)$") or ""
end

--
-- findTemplate
-- look into layout for a matching ref.
-- the content must be markdown
-- content= publications:sebastien/x:index.md
-- will match
-- layout= publications:sebastien/x:index.html
-- layout= publications:sebastien:index-.html
-- layout= publications:index-.html
-- (worry about lang later)
-- NOTE the outpath and url extension will becomde  the extension of  the template.
--
function findTemplates(family,id,file)
    -- assume this is markdown
    local ref=makeRef(family,id,file)
    local ext=".html"
    while(true) do
        local yref=makeRef(family,id,replaceExtension(file,ext))
        print("*** find templates for content "..ref.."  as layout "..yref)
        local y=layout[yref]
        if y then return y end
        if id=="" then
            print("**** no template  found for content "..ref)
            return
        end
        id=id:match("([^/]*)/[^/]-$") or ""
        ext="-.html"
    end
end

--
-- complete  missing  languages
--
-- content is assumed to be in all languages.
-- non markdown: copy inpath, outpath url to save file duplication.
-- markdown    : copy inpath, ....
-- 
--
function completeMissingLang(c)
    local ref=makeRef(c.family,c.id,c.file)
    print("*** completing  missing languages "..ref)
    return "a finir"
end

--
-- merge un content t d'une nouvelle langue dans le  contenu  c
-- mustbe the  same:   
-- merge un  content ou  un layout...
--
function mergeContent(c,t)
    print("*** merging into "..makeRef(c.family,c.id,c.file).." ***")
    -- verification cles qui doivent  correspondre
    for _,k  in ipairs({'family','id','file','mime','template'}) do
        if c[k]~=t[k] then
            print("*** unable to  merge  key "..k.." ***")
            for L,p in pairs(c.inpath)  do print("*** inpath["..L.."] = "..p) end
            for L,p in pairs(t.inpath)  do print("*** inpath["..L.."] = "..p) end
        end
    end
    -- on ajoute les infos de langue
    --  verification lang pas deja  defini
    for L,_ in pairs(t.lang) do
        if c.lang[L] then
            print("*** unable  to merge, lang defined 2x!")
            for l,p in pairs(c.inpath)  do print("*** inpath["..l.."] = "..p) end
            for l,p in pairs(t.inpath)  do print("*** inpath["..l.."] = "..p) end
            return true
        end
        for _,j in ipairs({'lang','inpath','isdef','outpath','url','params','html'})  do
            if t[j] and t[j][L]~=nil then
                if not c[j] then print("*** unable to merge,  key "..j.." absent from "..c.inpath[L]) return true end
                c[j][L]=t[j][L]
            else
            end
        end
    end
    return  false
end


--
-- output path for a specific content or layout
-- this will be used as outpath, from  config.outputDir/
-- and as url from config.baseURL/
-- this is for a specific language.
-- optional:  ext ifthere  will replace file ext
--
function makeOutputPath(c,L,ext)
    local f=c.file
    if ext then f=replaceExtension(c.file,ext) end
    local p=table.concat({config.langPath[L],c.family,c.id,f},"/")
    -- enleve les //// colles pour rien,  ou un  premier /
    return p:gsub("/+","/"):gsub("^/+","")
end



--   inpath[L] : fichier source complet
--   outpath[L]  : emplacement du fichier output  genene ou copie
--   url[L]   : url final
--   -> les 3 suivants sont seulement  si markdown.
--     template : layout template a appliquer sur le inpath pour le 'generer'  ou nil si pas besoin
--     params[L] : parametres obtenus du fichier yaml pour cette  langue
--              >>> un param xyz sera utilise peut  importe  la langue du content (L),  c'est le  defaut
--              >>> un param xyz-fr va definir  prioritairement le tag xyz si L=fr, sinon rien du tout.
--     html[L] :  parametres obtenus du yaml
function addContentFile(basepath,fullpath)
    -- extraire base,type,id,file a partir du path
    -- mime ne peut pas  changer entre differentes traductions --
    local mime=mimetypes.guess(basepath..fullpath) or ""
    local D,F,L,Def=splitPath(fullpath,config.langPath,config.langDefaut)

    if not D then return  end  -- skip this file!

    print("***** addContentFile "..fullpath.." ("..L..") "..(Def and "defaut!"  or "").." *****");

    local _,_,family,id=D:find("([^/]*)/?(.-)$")
    local ref=makeRef(family,id,F)

    inpath=basepath.."/"..fullpath

    local t={
        family=family,
        id=id,
        file=F,
        mime=mime,
    }
    t.lang={}
    t.lang[L]=true
    t.inpath={}
    t.inpath[L]=inpath
    t.isdef={}
    t.isdef[L]=Def -- is this a default language (no explicit language code?)

    t.outpath={}
    t.url={}

    -- process yaml if file is .md
    if mime=="text/x-markdown" then
        local info=readyaml(inpath)
        --for k,v in pairs(info) do t[k]=v end -- normalement params et html
        -- find  template
        t.params={}
        t.params[L]=info.params
        t.html={}
        t.html[L]=info.html

        -- look for template
        local tmp=findTemplates(t.family,t.id,t.file)
        if not tmp then
            print(red("*** content "..ref.." has no template!"))
        else
            tmp.isTemplate=true -- marque  ce  layout  comme etant  un template, donc pas de copie
            t.template=tmp
            local p=makeOutputPath(t,L,"."..getExtension(tmp.file))
            t.outpath[L]=config.outputDir.."/"..p
            t.url[L]=config.baseURL.."/"..p
        end
    else
        local p=makeOutputPath(t,L)
        t.outpath[L]=config.outputDir.."/"..p
        t.url[L]=config.baseURL.."/"..p
    end

    -- output file
    -- config.outputDir / [lang] / family / id / file  si pas markdown (sinon file->templatefile)
    -- outURL
    -- config.baseURL   / [lang] / family / id / file  si pas markdown (sinon file->templatefile)

    -- ajoute au  contenu
    if content[ref] then
        if mergeContent(content[ref],t)  then os.exit(0) end
    else
        content[ref]=t
    end

--   template : fichier template a appliquer sur le inpath pour le 'generer'  ou nil si pas besoin
--   outpath[L]  : emplacement du fichier output  genene ou copie
--   url[L]   : url final
end


function addLayoutFile(basepath,fullpath)
    -- extraire base,type,id,file a partir du path
    -- mime ne peut pas  changer entre differentes traductions --
    local mime=mimetypes.guess(basepath..fullpath) or ""
    local D,F,L,Def=splitPath(fullpath,config.langPath,config.langDefaut)

    print("***** addLayoutFile "..fullpath.." ("..L..") *****");

    local _,_,family,id=D:find("([^/]*)/?(.-)$")
    local ref=makeRef(family,id,F)

    inpath=basepath.."/"..fullpath

    local t={
        family=family,
        id=id,
        file=F,
        mime=mime,
    }
    t.lang={}
    t.lang[L]=true
    t.inpath={}
    t.inpath[L]=inpath
    t.isdef={}
    t.isdef[L]=Def -- is this a default language (no explicit language code?)

    -- on suppose qu'on ne sera pas  un  template... mais attention a isTemplate
    local p=makeOutputPath(t,L)
    t.outpath={}
    t.url={}
    t.outpath[L]=config.outputDir.."/"..p
    t.url[L]=config.baseURL.."/"..p

    -- ajoute au layout
    if layout[ref] then
        if mergeContent(layout[ref],t)  then os.exit(0) end
    else
        layout[ref]=t
    end

    -- special : si lang="",  then template applies to all lang
    --layout[#layout+1]={ filepath=path,family=family,id=id,file=file,mime=path.mime,lang=lang }
    --print("       FAM=["..family.."] ID=["..id.."] F=["..file.."] LANG=("..lang..")")
end










--function toto(x) return 2*x end
--kkk=1234567

--f=io.open("base.html","r")
--txt=f:read("*all")


--blub={"a","bb","ccc","dddd"}
--name = "leafo"
--items = { "Shoe", "Reflector", "Scarf" }

--print(insert("base.html"))
--print(insert("publication.html",{nom="seb"}))





-------------
-------------
-------------  GO!
-------------
-------------


--
-- pour les templates
--
config=readyaml(table.concat({inputDir,"config.md"},"/"))
-- keep only the params
if config then
    config=config.params
else
    print("no config.md. Please add one,  with  at least local: base URL,")
    print("make sure to specify outputDir  and baseURL")
    os.exit(0)
end


if profilParam then
    print("===  using  config profil ",profilParam)
    if config.profil and config.profil[profilParam] then
        -- remplace dans config  les parametres definis par le profil
        for  k,v  in  pairs(config.profil[profilParam]) do
            config[k]=v
        end
    else
        print("=== pas de profil "..profilParam.." in  config")
        if config.profil  then
            print("=== profil(s)  disponible(s) ===")
            for  k,v  in  pairs(config.profil) do
                print("   "..k)
            end
        else
            print("=== aucun profil disponible ===")
        end
        os.exit(0)
    end
end

-- update config from args
if outputDir then config.outputDir=outputDir end
if baseURL then config.baseURL=baseURL end

-- current  config
for  k,v  in  pairs(config) do
    if type(v)=="string" then
        print("=== config: "..k.." is "..v)
    end
end

-- lang
-- langPath : [lang]=repertoire pour chaque  lang.
-- langDefaut  : un des lang.
-- si  rien, on assume francais  et  repertoire ""
print("=== lang :")
if not config.langPath then config.langPath={fr=""} end
if not config.langDefaut  then config.langDefaut="fr" end
for  k,v  in  pairs(config.langPath) do print("lang ["..k.."] = ["..v.."]") end

-- on doit avoir au minimum...
-- config.outputDir
-- config.baseURL

if config.baseURL == nil then
    print("config.md has no baseURL");
    os.exit(0)
end

if config.outputDir == nil then
    print("config.md has no outputDir");
    os.exit(0)
end

print("=== baseURL : "..config.baseURL)
print("=== outputDir : "..config.outputDir)

--
-- scandir for  documents...
--
-- content/.../prefix.suffix     -> lang=""
-- content/.../prefix.gg.suffix  -> lang=gg
--
-- -> vrai pour tous les types de fichiers.
--

print("---- scan layout ----")
layout={}
scandir(inputDir.."/layout","",addLayoutFile)

--[[
for ref,c in  pairs(layout) do
    print("************ LAYOUT "..ref)
    dumpTable(c)
end
]]


print("---- scan content ----")
content={}
scandir(inputDir.."/content","",addContentFile)


--print("---- complete all  missing  languages ----")
--for ref,c in  pairs(content) do completeMissingLang(c)  end

--[[
for ref,c in  pairs(content) do
    print("************ CONTENT "..ref)
    dumpTable(c)
end
]]

--
-- flush current site
--

if flush then
    print("---- FLUSHING "..config.outputDir.." ----")
    os.execute("rm -rf "..config.outputDir)
end

--
-- a few  checks...
--
-- check  that  there is no  duplicate file  between content  and layout (a:b:c and a:b:c)
for k,c in pairs(content)  do
    if layout[k] and not layout[k].isTemplate then
        local zc={}
        local zl={}
        for L,i in pairs(c.inpath)  do zc[#zc+1]=i end
        for L,i in pairs(layout[k].inpath) do zl[#zl+1]=i end
        print(red("*** COLLISION CONTENT ( "..table.concat(zc," , ").." ) and LAYOUT ( "..table.concat(zl," , ").." )"))
    end
end



print("---- solving for  missing languages ----")
--
-- all  content must  have  a lang[L] true for all L in config.langPath
-- si  un  langage est absent, on utilise un autre langage qui a isdef[L]  (qui  est  default)
-- si on ajoute, on doit
-- copier inpath, html, params
-- set isdef=false,  lang=true
-- definir outpath et  url  correctement  (avec  le bon  langage)
--
--
function ajusteLang(which,k,t)
    -- is there a defaut content/template?  if so, set LD
    local  LD=nil
    for L,_ in pairs(config.langPath) do
        if t.isdef[L] then LD=L end
    end
    --  check each  language
    for L,_ in pairs(config.langPath) do
        if not t.lang[L] then
            if LD then
                print(yellow("*** "..which.." "..k.." has no lang="..L..". Using default  content."))
                t.inpath[L]=t.inpath[LD]
                if t.html then t.html[L]=t.html[LD] end
                if t.params then t.params[L]=clone(t.params[LD]) end  -- meme table NOT OK (on peut  changer un  tag a  cause  de tag-fr)
                t.isdef[L]=false
                t.lang[L]=true
                -- when using  a template,  use the extension of template (assumed .html), otherwise use as is.
                t.url[L]=config.baseURL.."/"..makeOutputPath(t,L,t.template and ".html")
                t.outpath[L]=config.outputDir.."/"..makeOutputPath(t,L,t.template and ".html")
                --dumpTable(t)
            else
                print(red("*** "..which.." "..k.." has no lang="..L.." and no default content. SKIP!"))
            end
        else
            print(green("*** "..which.." "..k.." has lang="..L.."."))
        end
    end
end

for k,t in pairs(content) do ajusteLang("content",k,t) end
for k,t in pairs(layout) do ajusteLang("layout",k,t) end

print("---- solving for multilingual params in content ----")
-- copy language tags .. xyz-fr -> xyz  if L=fr
for k,t in  pairs(content) do
    if t.mime=="text/x-markdown" then
        for L,_ in pairs(config.langPath) do
            for pn,pv in pairs(t.params[L]) do
                local pname,plang=pn:match("(.*)-(%a+)$")
                if pname and plang and plang==L then
                   t.params[L][pname]=pv
                end
            end
        end
    end
end



--  pour  le template support (utilisation  du  content  dans les template)
initialize()

print("---- copy/generate layout files (or generate if  html without content) ----")
for k,t in pairs(layout) do
    for L,_ in pairs(config.langPath) do
        if not t.lang[L] then
            print(red("LAYOUT "..k.." has no lang="..L.."."))
        else
            if not t.isTemplate then
                if t.mime=="text/html" then
                    print(yellow("LAYOUT "..k.." has no associated content. Using as empty template."))
                    writefile(t.outpath[L],insert(t.inpath[L],t,{L=L}))
                else
                    print("LAYOUT "..k.." COPY "..(t.inpath[L]).." >>> "..(t.outpath[L]))
                    doCopyFile(t.inpath[L],t.outpath[L])
                end
            end
        end
    end
end


print("---- copy/generate content files ----")


for k,t in pairs(content) do
    if t.mime=="text/x-markdown" then
        if not t.template then
            print(red("*** content "..k.." has no template. SKIP!"))
        else
            for L,_ in pairs(config.langPath) do
                if not t.lang[L] then
                    print(red("*** content "..k.." has no lang="..L.."."))
                else
                    if not t.template.inpath[L] then
                        print(red("*** content "..k.." has no template for lang="..L.."."))
                    else
                        print("CONTENT "..k.." GENERATE from "..(t.template.inpath[L]).." >>> "..(t.outpath[L]))
                        writefile(t.outpath[L],insert(t.template.inpath[L],t,{L=L}))
                    end
                end
            end
        end
    else
        for L,_ in pairs(config.langPath) do
            if not t.lang[L] then
                print(red("*** content "..k.." has no lang="..L.."."))
            else
                print("CONTENT "..k.." COPY "..(t.inpath[L]).." >>> "..(t.outpath[L]))
                doCopyFile(t.inpath[L],t.outpath[L])
            end
        end
    end
end


print("---- done----")

