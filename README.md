# luaweb



## Getting started

## Description

Static web site compiler, based on lua.


## Installation

Lua5.1 is required.

sudo  apt  install lua5.1 luarocks
sudo luarocks install etlua
sudo luarocks install i18n
sudo luarocks install luafilesystem
sudo luarocks install markdown
sudo luarocks install mimetypes
sudo luarocks install yaml

Dependencies:
* etlua ( https://github.com/leafo/etlua )
* i18n
* luafilesystem
* markdown
* mimetypes
* yaml


## Usage

./src/luaweb.lua -i pathToSourceDir  -o pathToOutputDir

## Support

## Authors and acknowledgment
S.Roy (roys@iro.umontreal.ca)

## License
GPLV3

